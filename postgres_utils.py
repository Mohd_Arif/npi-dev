import psycopg2
import psycopg2.extras

config = None

def get_cursor():
    # Should read params from a config file
    conn = psycopg2.connect(
        host="localhost",
        port="5432",
        dbname="npi-local",
        user="postgres",
        password="postgres")

    # create cursor
    return conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

def get_cursor_to_write():
    # Should read params from a config file
    conn = psycopg2.connect(
        host="localhost",
        port="5432",
        dbname="npi-local",
        user="postgres",
        password="postgres")

    # create cursor
    return conn

def create_data(statement):
    conn = None
    data = None
    try:
        cunn = get_cursor_to_write()
        cur = cunn.cursor()
        #execute statement
        cur.execute(statement)
        data = cur.statusmessage
        #close curoser
        cunn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return data

def get_all(statement):
    conn = None
    data = None
    try:
        cur = get_cursor()
        #execute statement
        cur.execute(statement)
        data = cur.fetchall()
        #close curoser
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return data

def get_by_id(statement, id):
    conn = None
    record = None
    try:
        cur = get_cursor()

        #execute statement
        cur.execute(statement, {'id': id})
        record = cur.fetchone()

        #close curoser
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return record

def insert_program(name):
    conn = None
    program = None
    try:
        cur = get_cursor()

        #execute statement
        cur.execute("INSERT INTO programs (name) VALUES (%(name)s)", {'name': name})
        cur.execute("SELECT id, name from programs where name = %(name)s", {'name': name})
        program = cur.fetchone()

        #close curoser
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return program

def insert_product(name):
    conn = None
    product = None
    try:
        cur = get_cursor_to_write()

        #execute statement
        cur.execute("INSERT INTO products (name) VALUES (%(name)s)", {'name': name})
        cur.execute("SELECT id, name from products where name = %(name)s", {'name': name})
        product = cur.fetchone()

        #close curoser
        cur.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return product

def set_config(config):
    #TODO
    return None

