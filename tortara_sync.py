import psycopg2
import psycopg2.extras
import json

config = None

def get_cursor():
    # Should read params from a config file
    conn = psycopg2.connect(
        host="kineo.ciena.com",
        port="5432",
        dbname="totara_ciena_reporting",
        user="totara_ciena_reporting",
        password="99pZ8mKB\"HmF7uX")

    # create cursor
    return conn.cursor()

def get_cursor_to_write():
    # Should read params from a config file
    conn = psycopg2.connect(
        host="localhost",
        port="5432",
        dbname="npi-local",
        user="postgres",
        password="postgres")

    # create cursor
    return conn


def get_all(fields):
    conn = None
    data = None
    try:
        cur = get_cursor()
        statement=f'SELECT {fields} FROM mdl_prog;'
        #execute statement
        cur.execute(statement)
        data = cur.fetchall()
        #close curoser
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return data

def create_data():
    conn = None
    data = None
    try:
        columns = ["id", "category", "sortorder", "fullname", "shortname", "idnumber", "summary", "endnote", "visible", "availablefrom", "availableuntil", "available", "timecreated", "timemodified", "usermodified", "icon", "exceptionssent", "audiencevisible", "certifid", "assignmentsdeferred", "allowextensionrequests", "enrolselfenabled", "enrolselfdueenabled", "enrolselfdueamount", "enrolselfdueunits"]
        fieldsQuery = ", ".join(columns);
        print(fieldsQuery,columns,type(columns));
        fetch_programs = get_all(fieldsQuery);
        value = ""
        i=0
        for program in fetch_programs:
            i += 1
            values = []
            valToappend = "null"
            index = 0;
            for col in columns:
                print(index,col)
                if program[index] != None:
                    try:
                        valToappend = ""+program[index]+""
                        valToappend = "E'"+valToappend.replace("'", "\\'")+"'"
                    except:
                        valToappend = json.dumps(program[index])
                values.append(valToappend)
                index = index + 1

            tempQuery = ", ".join(values)
            if len(fetch_programs) == i:
                value += "("+tempQuery+")"
            else:
                value += "("+tempQuery+"),"

        query = f'insert into mdl_prog ({fieldsQuery}) values {value}'
        print(query)
        
        cunn = get_cursor_to_write()
        cur = cunn.cursor()
        #execute statement
        cur.execute(query)
        data = cur.statusmessage
        #close curoser
        cunn.commit()
        cur.close()
        return query
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
    return data

