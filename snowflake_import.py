import snowflake.connector
import configparser
from cryptography.fernet import Fernet
from pathlib import Path
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.dialects.postgresql import insert

CONFIGPATH = str(Path.home()) + '/.snowflake/snowflake.conf'
STOREPATH = '/usr/local/etc/sf_store.bin'
SF_QUERY = ""

def read_store(key):
    store_cipher = Fernet(key)
    with open(STOREPATH, 'rb') as file_object:
        for line in file_object:
            encrypted_store = line
    return bytes(store_cipher.decrypt(encrypted_store)).decode("utf-8")

def get_snowflake_data():
    df = None

    config = configparser.ConfigParser()
    config.read(CONFIGPATH)
    config.sections()
    if 'AUTH' not in config:
        print('Auth missing from configuration file')
        exit(2)
    sf_user = config['AUTH']['username']
    sf_key = config['AUTH']['key']
    sf_account = config['AUTH']['account']
    sf_warehouse = config['AUTH']['warehouse']
    sf_pass = read_store(sf_key)

    with snowflake.connector.connect(
        user=sf_user,
        password=sf_pass,
        account=sf_account,
	    warehouse=sf_warehouse
        ) as conn:
            cs = conn.cursor()
            cs.execute("USE WAREHOUSE SVC_WH")
            cs.execute("SELECT * FROM FLYBIP.REPOSITORY_ENTERPRISE_SHARED.WORKER_BASIC_INFO_V")
            df = cs.fetch_pandas_all()
            print(f"Fetched {len(df)} rows and {len(df.columns)} columns of data from snowflake.")
            print("Showing first 10 rows:")
            print(df.head(10))

    return df

def save_to_postgres(df):
    engine = create_engine('postgresql://postgres:postgres@localhost:5432/npi')
    meta = sqlalchemy.MetaData()
    meta.bind = engine
    meta.reflect(views=True)

    def upsert(table, conn, keys, data_iter):
        upsert_args = {"constraint": "employees_pkey"}
        for data in data_iter:
            data = {k: data[i] for i, k in enumerate(keys)}
            upsert_args["set_"] = data
            insert_stmt = insert(meta.tables[table.name]).values(**data)
            upsert_stmt = insert_stmt.on_conflict_do_update(**upsert_args)
            conn.execute(upsert_stmt)

    with engine.connect() as conn:
        df.to_sql(
            "employees",
            con=conn,
            if_exists="append",
            method=upsert,
            index=False,
        )

def main():
    df = get_snowflake_data()
    save_to_postgres(df)
    print("Done. Exiting.")

if __name__ == "__main__":
    main()
