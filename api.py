from threading import Timer, Thread
import flask
from flask import make_response, request, jsonify
import postgres_utils as pgutils
import tortara_sync as program_sync
import threading
import requests
from lxml import etree
import json
import xmltodict

import base64

import zlib
base_path = 'C:/Users/marif/Documents/DEV_PROJECT/npi-dev'

app = flask.Flask(__name__)
app.config["DEBUG"] = True


def call_at_interval(time, callback, args):
    while True:
        timer = Timer(time, callback, args=args)
        timer.start()
        timer.join()


def setInterval(time, callback, *args):
    Thread(target=call_at_interval, args=(time, callback, args)).start()


def foo():
    print("hello1")


# A route to return all of the available courses.
@app.route('/api/v1/resources/courses', methods=['GET'])
def get_courses():
    try:
        name = request.args.get("name")
        jsonify(name)
        print(name)
        column = "id"
        order = "DESC"
        courses = []
        if(name != "undefined"):
            name = json.loads(name)
            print(name)
            print(type(name))
            try:
                if "column" in name:
                    column = name['column']
                    del name['column']
                if "order" in name:
                    order = name['order']
                    del name['order']
            except:
                pass
            query = "SELECT id, display_name FROM course"
            subquery = createQuery(name)
            if len(subquery):
                subquery = " WHERE "+subquery
            finalQry = query+subquery+f" ORDER BY {column} {order} limit 20"
            print(finalQry)
            courses = pgutils.get_all(finalQry)
        else:
            courses = pgutils.get_all(f"SELECT id, display_name FROM course ORDER BY {column} {order} limit 20")
        print(courses)
        # courses = [];
        if(not courses):
            return jsonify({"success": False, "data": [], "message": "Courses not found"}), 200
        response = jsonify({"success": True, "data": courses,
                           "message": "Courses fetched successfully"})
        response.headers.add("Access-Control-Allow-Origin", "*")
        return response, 200
    except ValueError:
        print(ValueError)
        return jsonify({"success": False, "message": "Something went wrong"}), 500

# A route to return the course with the specified id.


@app.route('/api/v1/resources/courses/<id>', methods=['GET'])
def get_course_by_id(id):
    try:
        course = None
        course = pgutils.get_by_id(
            "SELECT id, name FROM courses where id = %(id)s", id)
        if(not course):
            return jsonify({"success": False, "data": [], "message": "Course not found"})
        return jsonify({"success": True, "data": course, "message": "Course fetched successfully"})
    except ValueError:
        print(ValueError)
        return jsonify({"success": False, "message": "Something went wrong"})

# A route to return all of the available programs.


@app.route('/api/v1/resources/programs', methods=['GET'])
def get_programs():
    try:
        name = request.args.get("name")
        jsonify(name)
        print(name)
        column = "id"
        order = "DESC"
        programs = []
        if(name != "undefined"):
            name = json.loads(name)
            print(name)
            print(type(name))
            try:
                if "column" in name:
                    column = name['column']
                    del name['column']
                if "order" in name:
                    order = name['order']
                    del name['order']
            except:
                pass
            query = "SELECT id, fullname FROM mdl_prog"
            subquery = createQuery(name)
            if len(subquery):
                subquery = " WHERE "+subquery
            finalQry = query+subquery+f" ORDER BY {column} {order} limit 20"
            print(finalQry)
            programs = pgutils.get_all(finalQry)
        else:
            programs = pgutils.get_all(f"SELECT id, fullname FROM mdl_prog ORDER BY {column} {order} limit 20")
        if(not programs):
            response = jsonify({"success": False, "data": [], "message": "Programs not found"})
            response.headers.add("Access-Control-Allow-Origin", "*")
            return response
        response = jsonify({"success": True, "data": programs,
                           "message": "Programs fetched successfully"})
        response.headers.add("Access-Control-Allow-Origin", "*")
        return response,200
    except ValueError:
        print(ValueError)
        return jsonify({"success": False, "message": "Something went wrong"}), 500

# A route to get a program with the specified id.


@app.route('/api/v1/resources/programs/<id>', methods=['GET'])
def get_program_by_id(id):
    program = None
    try:
        program = pgutils.get_by_id(
            "SELECT id, name FROM program where id = %(id)s", id)
        print(program)
        if(not program):
            return jsonify({success: false, data: [], message: "Programs not found"})
        return jsonify({success: true, data: program, message: "Programs fetched successfully"})
    except:
        return jsonify({success: false, message: "Something went wrong"}), 500

#  A route to create a program.


@app.route('/api/v1/resources/products', methods=['POST'])
def create_product():
    try:
        product = None
        product = pgutils.insert_product(name)
        return make_response(jsonify({success: true, data: [], message: "Product added successfully"}), 201)
    except:
        return jsonify({success: false, message: "Something went wrong"})

# A route to edit a program with the specified id.


@app.route('/api/v1/resources/programs/<id>', methods=['PUT'])
def edit_program(id):
    try:
        program = None
        program = pgutils.get_by_id(
            "SELECT id, name FROM program where id = %(id)s", id)
        if(not program):
            return jsonify({success: false, data: [], message: "Programs not found"})
        return jsonify({success: true, data: program, message: "Programs updated successfully"})
    except:
        return jsonify({success: false, message: "Something went wrong"})


# A route to delete a program with the specified id.
@app.route('/api/v1/resources/programs/<id>', methods=['DELETE'])
def remove_program(id):
    try:
        program = None
        program = pgutils.get_by_id(
            "DELETE FROM program where id = %(id)s", id)
        if(not program):
            return jsonify({success: false, data: [], message: "Program not found"})
        return jsonify({success: true, data: program, message: "Program deleted successfully"})
    except:
        return jsonify({success: false, message: "Something went wrong"})
    return make_response(jsonify({}), 204)


def get_learning():
    # get all courses enrolled
    wstoken = 'f69ace6d326fcfb9ad1d01b7e0891a3d'
    wstoken2 = 'SHmw9BVeCPhcdkfKtijVZTtG8FvXtmfT'
    user = "jcripps@ciena.com"
    userid = "jcripps@ciena.com"
    coursesres = requests.get(
        'https://ciena.uat.us.kineo.com/webservice/rest/server.php?wstoken=NHCoiIE4Jzc8VN9rDp46tuduUvwVxVrI&wsfunction=core_course_get_courses')
    xml = xmltodict.parse(coursesres.content)
    enrolled_courses = json.dumps(xml)
    enrolled_courses = json.loads(enrolled_courses)
    # res = requests.get('https://learning.ciena.com/api/api.php?action=program&wstoken={wstoken}&user={user}'.format(wstoken=wstoken, user=user))
    # print("**********hello0000000******************",type(enrolled_courses),enrolled_courses['RESPONSE'].keys())
    # return enrolled_courses;
    finalData = []
    finalResp = {}
    for element in enrolled_courses['RESPONSE']['MULTIPLE']['SINGLE']:
        obj = {}
        for key in element['KEY']:
            try:
                print(key.keys(), key['VALUE'])
                obj[key['@name']] = key['VALUE']
            except:
                print(key.keys(), key['MULTIPLE'])
                obj[key['@name']] = key['MULTIPLE']

        finalData.append(obj)

    return finalData
    # programs = {
    #     "": dict((x.xpath('KEY[@name="id"]/VALUE/text()')[0], {
    #         "fullname": x.xpath('KEY[@name="fullname"]/VALUE/text()')[0],
    #         "idnumber": x.xpath('KEY[@name="idnumber"]/VALUE/text()')[0]
    #     }) for x in enrolled_courses)
    # }
    # print(programs)
    # programs_indexed = {}
    # for program in user['profile']['mdl_prog_completions']:
    #     programs[program['programid']] = {}
    #     programs_indexed[program['programid']] = program

    # for i, course in programs[''].items():
    #     for coursecomp in user['profile']['mdl_course_completions']:
    #         if i == coursecomp['course']:
    #             programs[''][i]['status'] = coursecomp['status']

    # for courseset in user['profile']['mdl_prog_courseset']:
    #     programs[courseset['programid']][courseset['id']] = {}

    # for xcourse in user['profile']['mdl_prog_courseset_course']:
    #     for i, prog in programs.items():
    #         if xcourse['coursesetid'] in prog:
    #             programs[i][xcourse['coursesetid']][xcourse['courseid']] = {}

    # courses_index = {}
    # for course in user['profile']['mdl_course']:
    #     courses_index[course['id']] = course

    # course_completions_indexed = {}
    # for coursecomp in user['profile']['mdl_course_completions']:
    #     course_completions_indexed[coursecomp['course']] = coursecomp
    # print(programs)
    # #print(course_completions_indexed)
    # for i, prog in programs.items():
    #     if i != '':
    #         for x, courseset in prog.items():
    #             for j, course in courseset.items():
    #                 programs[i][x][j].update(courses_index[j])
    #                 if j in course_completions_indexed:
    #                     programs[i][x][j].update(course_completions_indexed[j])
    #                 programs[i][x][j].update({'programname': programs_indexed[i]['fullname']})
    # #print("*******programs",programs)
    # #print("********completion",course_completions_indexed)
    # programs2 = {"": []}
    # #print(programs2)
    # for idx, program in programs.items():
    #     if idx == '':
    #        # print("**********************",program)
    #         for idx2, xcourse in program.items():
    #             programs2[''].append(xcourse)
    #         continue
    #     programobj = {}
    #     for idx2, xcourse in program.items():
    #         for idx3, course in xcourse.items():
    #             if idx not in programs2:
    #                 programs2[idx] = []
    #             programs2[idx].append(course)
    # print(programs2)


@app.route('/', methods=['GET'])
def home():
    programs = get_learning()
    getColumns = {"id": "id", "short_name": "shortname", "categoryid": "categoryid", "category_sort_order": "categorysortorder", "full_name": "fullname", "display_name": "displayname", "id_number": "idnumber", "start_date": "startdate", "end_date": "enddate", "num_section": "numsections", "visible": "visible", "course_type": "coursetype",
                  "force_theme": "forcetheme", "lang": "lang", "completion_notify": "completionnotify", "completion_start_onenrol": "completionstartonenrol", "enable_completion": "enablecompletion", "group_mode": "groupmode", "group_mode_force": "groupmodeforce", "default_grouping_id": "defaultgroupingid", "time_created": "timecreated", "time_modified": "timemodified"}
    columns = getColumns.keys()
    sql_column = " ,".join(columns)
    value = ""
    i = 0
    for course in programs:
        i += 1
        values = []
        valToappend = "null"
        for column in columns:
            print(course[getColumns[column]])
            if course[getColumns[column]] != None:
                try:
                    valToappend = ""+course[getColumns[column]]+""
                    valToappend = "E'"+valToappend.replace("'", "\\'")+"'"
                except:
                    valToappend = json.dumps(course[getColumns[column]])
            values.append(valToappend)

        tempQuery = ", ".join(values)
        if len(programs) == i:
            value += "("+tempQuery+")"
        else:
            value += "("+tempQuery+"),"

    query = f'insert into course ({sql_column}) values {value}'
    print(query)
    program = pgutils.create_data(query)
    # print({programs});
    return jsonify({"data": programs})


@app.route("/resync")
def resync():

    # """
    #import os
    res = True
    getColumns = {
        "email_optin": "emailoptin",
        "company": "company",
        "id": "id",
        "city": "city",
        "confirmed": "confirmed",
        "first_name": "firstname",
        "company_group": "CompanyGroup",
        "address2": "Address2",
        "signup_code": "signupcode",
        "pager": "Pager",
        "email": "email",
        "user_name": "username",
        "member_type_saml": "membertypesaml",
        "employee_id": "employeeid",
        "ciena_login_name": "CienaLoginName",
        "last_name": "lastname",
        "notes": "Notes",
        "phone1": "phone1",
        "fax": "Fax",
        "QA_records": "QARecords",
        "portal_country": "portalcountry",
        "is_portal_user": "isportaluser",
        "franchise": "franchise",
        "evaluator": "evaluator",
        "ciena_object_id": "CienaObjectID",
        "referral": "referral",
        "region": "region",
        "member_type": "membertype",
        "zip_code": "ZipCode",
        "country": "country",
        "parent_account_number": "parentaccountnumber",
        "state_or_province": "StateOrProvince",
        "industry": "industry",
        "title": "Title",
        "manager_email": "ManagerEmail"
    }

    res = requests.get(
        'https://learning.ciena.com/api/api.php?wstoken=f69ace6d326fcfb9ad1d01b7e0891a3d&action=user_list',
        data={},
        stream=True,timeout=1000)
    # print("***************",res)
    with open('temp.gz', 'wb') as f:
        for chunk in res.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
                #f.flush() commented by recommendation from J.F.Sebastian
    f.close()

    # """
    with open(base_path+'/temp.gz') as f:
        # newlist = base64.b64decode(f.read()).decode('zlib')
        newlistdata = zlib.decompress(base64.b64decode(f.read()))
        newlist = newlistdata.decode()
        # print(newlist[:100])
        users = json.loads(newlist[newlist.find('['):])
        #users = json.loads(newlist[newlist.find('users'):])[8:]
        # client = MongoClient('localhost')
        columns = getColumns.keys()
        sql_column = " ,".join(columns)
        value = ""
        i = 0
        for employee in users:
            i += 1
            values = []
            valToappend = "null"
            for column in columns:
                try:
                    print(employee[getColumns[column]])
                    if employee[getColumns[column]] != None:
                        try:
                            if len(employee[getColumns[column]]):
                                valToappend = ""+employee[getColumns[column]]+""
                                valToappend = "E'"+valToappend.replace("'", "\\'")+"'"
                            else:
                                valToappend = "null"
                        except:
                            valToappend = json.dumps(employee[getColumns[column]])
                except:
                    pass
                values.append(valToappend)
                
            tempQuery = ", ".join(values)
            if len(users) == i:
                value += "("+tempQuery+")"
            else:
                value += "("+tempQuery+"),"

        query = f'insert into employee ({sql_column}) values {value}'
        print(query)
        program = pgutils.create_data(query)
    # print('xx',xx)
    return jsonify({"data": query})

@app.route("/resyncprogram")
def resync_program():

    # """
    #import os
    res = True
    getColumns = {
        "email_optin": "emailoptin",
        "company": "company",
        "id": "id",
        "city": "city",
        "confirmed": "confirmed",
        "first_name": "firstname",
        "company_group": "CompanyGroup",
        "address2": "Address2",
        "signup_code": "signupcode",
        "pager": "Pager",
        "email": "email",
        "user_name": "username",
        "member_type_saml": "membertypesaml",
        "employee_id": "employeeid",
        "ciena_login_name": "CienaLoginName",
        "last_name": "lastname",
        "notes": "Notes",
        "phone1": "phone1",
        "fax": "Fax",
        "QA_records": "QARecords",
        "portal_country": "portalcountry",
        "is_portal_user": "isportaluser",
        "franchise": "franchise",
        "evaluator": "evaluator",
        "ciena_object_id": "CienaObjectID",
        "referral": "referral",
        "region": "region",
        "member_type": "membertype",
        "zip_code": "ZipCode",
        "country": "country",
        "parent_account_number": "parentaccountnumber",
        "state_or_province": "StateOrProvince",
        "industry": "industry",
        "title": "Title",
        "manager_email": "ManagerEmail"
    }

    programs = program_sync.create_data();
    print(programs)
    
    
    # columns = getColumns.keys()
    # sql_column = " ,".join(columns)
    # value = ""
    # i = 0
    # for employee in users:
    #     i += 1
    #     values = []
    #     valToappend = "null"
    #     for column in columns:
    #         try:
    #             print(employee[getColumns[column]])
    #             if employee[getColumns[column]] != None:
    #                 try:
    #                     if len(employee[getColumns[column]]):
    #                         valToappend = ""+employee[getColumns[column]]+""
    #                         valToappend = "E'"+valToappend.replace("'", "\\'")+"'"
    #                     else:
    #                         valToappend = "null"
    #                 except:
    #                     valToappend = json.dumps(employee[getColumns[column]])
    #         except:
    #             pass
    #         values.append(valToappend)
            
    #     tempQuery = ", ".join(values)
    #     if len(users) == i:
    #         value += "("+tempQuery+")"
    #     else:
    #         value += "("+tempQuery+"),"

    # query = f'insert into employee ({sql_column}) values {value}'
    # print(query)
    # program = pgutils.create_data(query)
    return jsonify({"data": programs})


def createQuery(obj):
    query = ""
    arr = []
    keys = obj.keys()
    for key in keys:
        if len(obj[key]):
            if(key == "is_employee"):
                query = f" {key} = {obj[key].lower()}"
            elif(key == "id"):
                query = f" {key}::varchar LIKE '{obj[key].lower()}%'"
            else:
                query = f" LOWER({key}) LIKE '{obj[key].lower()}%'"
            arr.append(query)

    query = " and ".join(arr)
    print(query)
    return query


@app.route('/api/v1/resources/employee', methods=['GET'])
def get_employee():
    try:
        name = request.args.get("name")
        jsonify(name)
        employee = []
        column = "id"
        order = "DESC"
        if(name != "undefined"):
            name = json.loads(name)
            print(name)
            print(type(name))
            try:
                if "column" in name:
                    column = name['column']
                    del name['column']
                if "order" in name:
                    order = name['order']
                    del name['order']
            except:
                pass
            query = "SELECT * FROM employee"
            subquery = createQuery(name)
            if len(subquery):
                subquery = " WHERE "+subquery
            finalQry = query+subquery+f" ORDER BY {column} {order} limit 20"
            print(finalQry)
            employee = pgutils.get_all(finalQry)
        else:
            employee = pgutils.get_all(f"SELECT * FROM employee ORDER BY {column} {order} limit 20")
        # print(employee)
        # employee = [];
        if(not employee):
            response = jsonify(
                {"success": False, "data": [], "message": "Employee not found"})
            response.headers.add("Access-Control-Allow-Origin", "*")
            return response
        response = jsonify({"success": True, "data": employee,
                           "message": "Employees fetched successfully"})
        response.headers.add("Access-Control-Allow-Origin", "*")
        return response, 200
    except ValueError:
        print(ValueError)
        return jsonify({"success": False, "message": "Something went wrong"}), 500

# get_learning();


app.run()
